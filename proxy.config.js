const proxy = [
    {
      context: '/api',
      target: 'http://api.compranalogos.com.br',
      pathRewrite: {'^/api' : ''}
    }
  ];
  module.exports = proxy;