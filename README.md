# Backend do Site Logos Veículos
## Tenologias usadas:
1. Angular7
2. HTML5
3. SCSS
4. Mdbootstrap versão 4
   

## Development server

Executa `ng serve` para iniciar server no desenvolvimento. Navigate to `http://localhost:4200/`. 

## Code scaffolding
Executa `ng generate component component-name` para criar um novo componente. alem, pode usar `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build
Executa `ng build` para compilar o projeto. Usa a bandeira `--prod` para compilar no modo produção.
