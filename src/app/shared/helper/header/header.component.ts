import { Component, OnInit } from '@angular/core';
import { LoopbackService } from '../../service/loopback.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private lb: LoopbackService, private router: Router) { }

  ngOnInit() {
  }

  onLogout() {
    Swal.fire({
      title: 'Você tem certeza?',
      text: 'Você sairá do sistema!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim, sair!'
    }).then((result) => {
      this.lb.signOut().subscribe(data => {
        localStorage.removeItem('accessBackend');
        this.router.navigate(['']);
      });
    });
  }
}
