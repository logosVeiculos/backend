import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { empty } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LoopbackService {

  url: string;
  constructor(private http: HttpClient) {
    this.url = 'http://localhost:3021/api';
  }

  /** Start Auth  */
  isLoggedIn() {
    return localStorage.getItem('accessBackend') ? true : false;
  }

  getAccessToken() {
    return JSON.parse(localStorage.getItem('accessBackend')).id;
  }

  getAccessTokenUserId() {
    return JSON.parse(localStorage.getItem('accessBackend')).userId;
  }

  signIn(data) {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    return this.http.post(`${this.url}/Users/login`, data, {headers: headers});
  }

  signOut() {
    const accessToken = this.getAccessToken();
    return this.http.post(`${this.url}/Users/logout?access_token=${accessToken}`, {});
  }
  /** End Auth */

  getTable(itens, limit = 0) {
    if (limit === 0) {
      return this.http.get(`${this.url}/${itens}`);
    } else {
      return this.http.get(`${this.url}/${itens}?filter[limit]=${limit}`);
    }
  }

  getCount(item) {
    return this.http.get(`${this.url}/${item}/count`);
  }

  getFindById(table, iten) {
    return this.http.get(`${this.url}/${table}/${iten}`,
    { headers:
      { 'Access-Control-Allow-Origin': '*' }
    }
    );
  }

  getFindBy(table, field, search, limit= 0, order= 'ASC') {
    if (limit === 0) {
      return this.http.get(`${this.url}/${table}?filter[where][${field}]=${search}`);
    } else {
      return this.http.get(`${this.url}/${table}?filter[where][${field}]=${search}&[filter][limit]=${limit}&filter[order]=id%20${order}`);
    }
  }

  getFindBy2(table, field1, search1, field2, search2, limit= 0) {
    if (limit === 0) {
      return this.http.get(`${this.url}/${table}?filter[where][${field1}]=${search1}&[where][${field2}]=${search2}`);
    } else {
      // tslint:disable-next-line:max-line-length
      return this.http.get(`${this.url}/${table}?filter[where][${field1}]=${search1}&[where][${field2}]=${search2}&[filter][limit]=${limit}`);
    }
  }

  getCountFindBy(item, field, search) {
    return this.http.get(`${this.url}/${item}/count?[where][${field}]=${search}`);
  }

  getPropostaByCarro(id: number) {
    return this.http.get(`${this.url}/carros/${id}/proposta/count`);
  }

/**
 * 
 * @param ofertas Int
 * @param status String
 */
  getTotalOfertas(ofertas: number, status = 'disponivel') {
    return this.http.get(`${this.url}/carros/count?[where][ofertas]=${ofertas}&filter[where][status]=${status}`);
  }

  postData(item, data) {
    return this.http.post(`${this.url}/${item}`, data, {headers:
      { 'Content-Type': 'application/json; charset=utf-8' }});
  }

  updateData(table, id, data) {
    return this.http.put(`${this.url}/${table}/${id}`, data, {headers:
      { 'Content-Type': 'application/json; charset=utf-8' }});
  }

  deleteData(item, id) {
    return this.http.delete(`${this.url}/${item}/${id}`);
  }

  getUser() {
    const accessToken = this.getAccessToken();
    const user_id = this.getAccessTokenUserId();
    const headers = new HttpHeaders({'Access-Control-Allow-Origin' : 'http://localhost:4200'});
    return this.http.get(`${this.url}/Users/${user_id}?access_token=${accessToken}`, {headers: headers});
  }

  getUserAll() {
    const accessToken = this.getAccessToken();
    return this.http.get(`${this.url}/Users?access_token=${accessToken}`);
  }

  getUserCount() {
    const accessToken = this.getAccessToken();
    const headers = new HttpHeaders({'Access-Control-Allow-Origin' : 'http://localhost:4200'});
    return this.http.get(`${this.url}/Users/count?access_token=${accessToken}`, {headers: headers});
  }

  /**
   * Images
   * @param data image upload
   */
  uploadImg(data) {
    console.log(data);
    const headers = new HttpHeaders({
      'Content-Type' : 'application/x-www-form-urlencoded',
      'Access-Control-Allow-Origin': '*',
    });
    return this.http.post(`${this.url}/storages/photos/upload`, data, {headers: headers});
  }

  /**
   * Images
   * @param data image upload
   */
  deleteImg(nome) {
    console.log(nome);
    const headers = new HttpHeaders({
      'Content-Type' : 'application/x-www-form-urlencoded',
      'Access-Control-Allow-Origin': '*',
    });
    return this.http.delete(`${this.url}/storages/photos/files/${nome}`);
  }
}
