import { Component, Input, OnInit } from '@angular/core';
import { LoopbackService } from '../../service/loopback.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit {

  @Input() carroId: number;
  carro: any;

  constructor(private lb: LoopbackService) { }

  ngOnInit() {
    this.onCarro(this.carroId);
  }

  onCarro(id) {
    this.lb.getFindById('carros', id)
    .subscribe(data => {
      this.carro = data;
      console.log(data);
    });
  }

}
