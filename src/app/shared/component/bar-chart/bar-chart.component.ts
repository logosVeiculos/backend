import { Component, OnInit } from '@angular/core';
import { LoopbackService } from '../../service/loopback.service';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss']
})
export class BarChartComponent implements OnInit {

  offers: any;
  nova: any;
  quentes: any;
  imperdivel: any;
  public chartType = 'bar';
  public chartDatasets: Array<any> = [
    { data: [65, 59, 157], label: 'Quantidade de Ofertas' },
  ];
  public chartLabels: Array<any> = ['Nova', 'Quentes', 'Imperdível'];
  public chartColors: Array<any> = [
    {
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)'
      ],
      borderWidth: 2,
    }
  ];

  public chartOptions: any = {
    responsive: true,
      scales: {
        xAxes: [{
          stacked: true
          }],
        yAxes: [
        {
          stacked: true
        }
      ]
    }
  };
  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }
  constructor(private lb: LoopbackService) { }

  ngOnInit() {
    this.totalOfertasNovas();
    this.totalOfertasQuentes();
    this.totalOfertasImperdivel();
  }

  totalOfertasQuentes(id = 2) {
    this.lb.getTotalOfertas(id).subscribe(data => {
      this.quentes = data['count'];
    });
  }
  totalOfertasNovas(id = 1) {
    this.lb.getTotalOfertas(id).subscribe(data => {
      this.nova = data['count'];
    });
  }
  totalOfertasImperdivel(id = 3) {
    this.lb.getTotalOfertas(id).subscribe(data => {
      this.imperdivel = data['count'];
    });
  }

}
