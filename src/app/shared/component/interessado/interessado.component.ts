import { Component, OnInit, Input } from '@angular/core';
import { LoopbackService } from '../../service/loopback.service';

@Component({
  selector: 'app-interessado',
  templateUrl: './interessado.component.html',
  styleUrls: ['./interessado.component.scss']
})
export class InteressadoComponent implements OnInit {

  @Input() id: number;
  output: any;

  constructor(private lb: LoopbackService) { }

  ngOnInit() {
    this.lb.getCountFindBy('proposta', 'carroId', this.id).subscribe(data => {
      this.output = data['count'];
    }, err => {
      console.log(err);
    });
  }
}
