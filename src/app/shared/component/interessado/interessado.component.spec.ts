import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InteressadoComponent } from './interessado.component';

describe('InteressadoComponent', () => {
  let component: InteressadoComponent;
  let fixture: ComponentFixture<InteressadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InteressadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InteressadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
