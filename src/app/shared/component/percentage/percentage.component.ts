import { Component, OnInit, Input, Output } from '@angular/core';
import { LoopbackService } from '../../service/loopback.service';
import { CanActivate } from '@angular/router/src/utils/preactivation';

@Component({
  selector: 'app-percentage',
  templateUrl: './percentage.component.html',
  styleUrls: ['./percentage.component.scss']
})
export class PercentageComponent implements OnInit {

  @Input() oferta: number;
  @Output() cant;

  constructor(private lb: LoopbackService) { }

  ngOnInit() {
    this.onSuccess(this.oferta);
  }

  onSuccess(id) {
    this.lb.getCountFindBy('carros', 'ofertas', id)
    .subscribe(data => {
      this.lb.getCount('carros')
      .subscribe(carro => {
        const r = (data['count'] / carro['count']) * 100;
        this.cant = this.round(r, 2);
      });
    });
  }

  round(value, precision) {
    const multiplier = Math.pow(10, precision || 0);
    return Math.round(value * multiplier) / multiplier;
  }
}
