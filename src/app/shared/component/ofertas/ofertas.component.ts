import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { LoopbackService } from '../../service/loopback.service';

@Component({
  selector: 'app-ofertas',
  templateUrl: './ofertas.component.html',
  styleUrls: ['./ofertas.component.scss']
})
export class OfertasComponent implements OnInit {

  @Input() oferta;
  @Output() nome = new EventEmitter;

  constructor(private lb: LoopbackService) { }

  ngOnInit() {
    // console.log('@@@@ Oferta', this.oferta);
    this.onOferta(this.oferta);
  }

  onOferta(item) {
    this.lb.getFindBy('ofertas', 'id', item)
    .subscribe(data => {
      this.nome = data[0]['nome'];
      // console.log('@@@@ Nome', data[0]['nome']);
    });
  }

}
