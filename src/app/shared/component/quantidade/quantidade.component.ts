import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { LoopbackService } from '../../service/loopback.service';

@Component({
  selector: 'app-quantidade',
  templateUrl: './quantidade.component.html',
  styleUrls: ['./quantidade.component.scss']
})
export class QuantidadeComponent implements OnInit {

  @Input() oferta: number;
  @Output() cant = new EventEmitter();

  constructor(private lb: LoopbackService) { }

  ngOnInit() {
    this.onSuccess(this.oferta);
  }

  onSuccess(id) {
    this.lb.getCountFindBy('carros', 'ofertas', id)
    .subscribe(data => this.cant = data['count']);
  }

}
