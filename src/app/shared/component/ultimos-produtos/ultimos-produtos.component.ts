import { Component, OnInit } from '@angular/core';
import { LoopbackService } from '../../service/loopback.service';

@Component({
  selector: 'app-ultimos-produtos',
  templateUrl: './ultimos-produtos.component.html',
  styleUrls: ['./ultimos-produtos.component.scss']
})
export class UltimosProdutosComponent implements OnInit {

  slides: any = [[]];

  constructor(private lb: LoopbackService) { }

  ngOnInit() {
    // this.slides1 = this.chunk(this.cards, 4);
    this.onProducts();
  }


  onProducts() {
    this.lb.getFindBy('carros', 'status', 'disponivel', 8, 'DESC')
    .subscribe(data => {
      this.slides = this.chunk(data, 4);
    });
  }

  chunk(arr, chunkSize) {
    let R = [];
    for (let i = 0, len = arr.length; i < len; i += chunkSize) {
      R.push(arr.slice(i, i + chunkSize));
    }
    console.log(R);
    return R;
  }


}
