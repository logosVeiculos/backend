import { Component, OnInit } from '@angular/core';
import { LoopbackService } from 'src/app/shared/service/loopback.service';

@Component({
  selector: 'app-chat-status',
  templateUrl: './chat-status.component.html',
  styleUrls: ['./chat-status.component.scss']
})
export class ChatStatusComponent implements OnInit {

  vendido: number;
  disponivel: number;

  public chartType = 'pie';
  public chartColors: Array<any> = [
    {
      backgroundColor: ['#F7464A', '#46BFBD'],
      hoverBackgroundColor: ['#FF5A5E', '#5AD3D1'],
      borderWidth: 10,
    }
  ];

  public chartOptions: any = {
    responsive: true
  };
  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }



  constructor(private lb: LoopbackService) { }

  ngOnInit() {
    this.totalVendido();
    this.totalDisponivel();
  }

  public round(value, precision) {
    const multiplier = Math.pow(10, precision || 0);
    return Math.round(value * multiplier) / multiplier;
  }

  public totalVendido() {
    this.lb.getCountFindBy('carros', 'status', 'vendido')
    .subscribe(data => {
      this.vendido = data['count'];
    });
  }
  public totalDisponivel() {
    this.lb.getCountFindBy('carros', 'status', 'disponivel')
    .subscribe(data => {
      this.disponivel = data['count'];
    });
  }
}
