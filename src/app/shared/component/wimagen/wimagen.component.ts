import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { LoopbackService } from '../../service/loopback.service';

@Component({
  selector: 'app-wimagen',
  templateUrl: './wimagen.component.html',
  styleUrls: ['./wimagen.component.scss']
})
export class WimagenComponent implements OnInit {

  @Input() carroId;
  @Output() img = new EventEmitter;
  flag;

  constructor(private lb: LoopbackService) { }

  ngOnInit() {
    this.url(this.carroId);
  }


  url(id) {
    // console.log('@@@ URL de Image', id);
    this.lb.getFindBy('images', 'carroId', id)
    .subscribe(data => {
      this.flag = data['length'];
      if (this.flag > 0) {
        this.img = data[0]['url'];
        // console.log('#### IMG', this.img);
      } else {
        // console.log('#### Sin Imagem');
      }
    });
  }

}
