import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WimagenComponent } from './wimagen.component';

describe('WimagenComponent', () => {
  let component: WimagenComponent;
  let fixture: ComponentFixture<WimagenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WimagenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WimagenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
