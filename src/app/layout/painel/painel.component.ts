import { Component, OnInit } from '@angular/core';
import { LoopbackService } from 'src/app/shared/service/loopback.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-painel',
  templateUrl: './painel.component.html',
  styleUrls: ['./painel.component.scss']
})
export class PainelComponent implements OnInit {

  ofertas: any;
  ofertasTotal: number;
  veiculo: any;

  headElements = ['ID', 'Imagem', 'Veículo', 'Ofertas', 'Valor', 'Ver'];
  isLoggedIn;
  constructor(
    private lb: LoopbackService,
    private router: Router
  ) {
    this.isLoggedIn = this.lb.isLoggedIn();
    if (this.isLoggedIn === false) {
      this.router.navigate(['']);
    }
  }
  ngOnInit() {
    this.ofertasStatus();
    this.getVeiculos5();
  }

  ofertasStatus() {
    this.lb.getTable('ofertas')
    .subscribe(offer => this.ofertas = offer);
    this.lb.getCount('carros')
    .subscribe(data => {
      this.ofertasTotal = data['count'];
    });
  }

  getVeiculos5() {
    this.lb.getFindBy('carros', 'status', 'disponivel', 5)
    .subscribe(data => {
      this.veiculo = data;
    });
  }
}
