import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { LoopbackService } from 'src/app/shared/service/loopback.service';
import { NgxSpinnerService } from 'ngx-spinner';
import swal from 'sweetalert';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  flag: boolean;
  formLogin: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private lb: LoopbackService,
    private spinner: NgxSpinnerService
  ) {
    this.flag = false;
    if (this.lb.isLoggedIn()) {
      this.router.navigate(['home']);
    }
  }

  ngOnInit() {
    this.buildForm();
  }

  onSubmit() {
    if (!this.formLogin.valid) {
      return;
    }
    this.spinner.show();
    setTimeout(() => {
      this.lb.signIn(this.formLogin.value)
      .subscribe(data => {
        this.spinner.hide();
        console.log('@@@ Access');
        console.log('JSON =>', JSON.stringify(data['id']));
        console.log(data);
        localStorage.setItem('accessBackend', JSON.stringify(data));
        swal('Yeah!!!!', 'Bem vindo ao Painel do Logos Veículos!', 'success');
        this.router.navigate(['painel']);
      }, err => {
        this.spinner.hide();
        swal('WTF!', 'E-mail ou senha errada!', 'error');
      });
    }, 2000);
  }

  onReset(email: string) {

  }

  buildForm() {
    this.formLogin = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  flagOpen() {
    this.flag = true;
  }

  flagClose() {
    this.flag = false;
  }
}
