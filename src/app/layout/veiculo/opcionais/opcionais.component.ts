import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { LoopbackService } from 'src/app/shared/service/loopback.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-opcionais',
  templateUrl: './opcionais.component.html',
  styleUrls: ['./opcionais.component.scss']
})
export class OpcionaisComponent implements OnInit {

  id: number;
  formOpcionais: FormGroup;
  carro;
  estado: string;
  cidade: string;
  oferta: string;
  isLoggedIn;

  constructor(
    private fb: FormBuilder,
    private lb: LoopbackService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.route.url.subscribe(u => {
      this.id = this.route.snapshot.params.id;
      this.onExits(this.id);
    });
    this.isLoggedIn = this.lb.isLoggedIn();
    if (this.isLoggedIn === false) {
      this.router.navigate(['']);
    }
  }

  ngOnInit() {
    this.buildForm();
  }

  getEstado(id) {
    this.lb.getFindBy('estados', 'id', id)
    .subscribe(data => this.estado = data[0]['nome']);
  }

  getCidade(id) {
    this.lb.getFindBy('cidades', 'id', id)
    .subscribe(data => this.cidade = data[0]['nome']);
  }

  getOferta(id) {
    this.lb.getFindBy('ofertas', 'id', id)
    .subscribe(data => this.oferta = data[0]['nome']);
  }

  buildForm() {
    this.formOpcionais = this.fb.group({
      arCondicionado: [''],
      arBag: [''],
      travasEletricas: [''],
      direcaoHidraulica: [''],
      brakeLight: [''],
      vidrosEletricos: [''],
      limpadorTraseiro: [''],
      freioABS: [''],
      radio: [''],
      dvdPlayer: [''],
      vidrosTraseirosEletricos: [''],
      espelhosRetrovisoresTraseiros: [''],
      bancosEnCouro: [''],
      altosFalantes: [''],
      gps: [''],
      alarme: [''],
      protetorDeCacamba: [''],
      sensorEstaciomamentoTraseiro: [''],
      carroId: [this.id]
    });
  }

  onSend() {
    if (!this.formOpcionais.valid) {
      return;
    }

    const form = this.formOpcionais.value;
    this.lb.postData('opcionais', form)
    .subscribe(data => this.router.navigate([`veiculos/passo3/${this.id}`]));
  }

  onExits(carro) {
    this.lb.getFindBy('carros', 'id', this.id)
    .subscribe(data => {
      this.carro = data;
      this.getEstado(data[0].estado);
      this.getCidade(data[0].cidade);
      this.getOferta(data[0].ofertas);
    });
  }
}
