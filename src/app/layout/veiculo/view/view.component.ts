import { Component, OnInit } from '@angular/core';
import { LoopbackService } from 'src/app/shared/service/loopback.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  id: number;
  carro: any;
  estado: string;
  cidade: string;
  oferta: string;
  imagen: any;
  opcionais: any;
  isLoggedIn;

  constructor(
    private lb: LoopbackService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.route.url.subscribe(u => {
      this.id = this.route.snapshot.params.id;
    });
    this.isLoggedIn = this.lb.isLoggedIn();
    if (this.isLoggedIn === false) {
      this.router.navigate(['']);
    }
  }

  ngOnInit() {
    this.onCarro(this.id);
    this.getImages(this.id);
    this.getOpcionais(this.id);
  }

  getEstado(id) {
    this.lb.getFindBy('estados', 'id', id)
    .subscribe(data => this.estado = data[0]['nome']);
  }

  getCidade(id) {
    this.lb.getFindBy('cidades', 'id', id)
    .subscribe(data => this.cidade = data[0]['nome']);
  }

  getOferta(id) {
    this.lb.getFindBy('ofertas', 'id', id)
    .subscribe(data => this.oferta = data[0]['nome']);
  }

  onCarro(id) {
    this.lb.getFindBy('carros', 'id', id).subscribe(data => {
      this.carro = data;
      this.getEstado(data[0].estado);
      this.getCidade(data[0].cidade);
      this.getOferta(data[0].ofertas);
    });
  }

  getImages (id) {
    this.lb.getFindBy('images', 'carroId', id)
    .subscribe(data => this.imagen = data);
  }

  getOpcionais (id) {
    this.lb.getFindBy('opcionais', 'carroId', id)
    .subscribe(data => {
      this.opcionais = data;
    });
  }
}
