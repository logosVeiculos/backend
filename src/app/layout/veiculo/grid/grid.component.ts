import { Component, OnInit, ViewChild } from '@angular/core';
import { LoopbackService } from 'src/app/shared/service/loopback.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { ModalDirective } from 'angular-bootstrap-md';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})

export class GridComponent implements OnInit {

  carros: any;
  isLoggedIn;
  count: number;
  @ViewChild('ofertaModal') ofertaModal: ModalDirective;
  @ViewChild('valorModal')  valorModal:  ModalDirective;
  table;
  id;
  mc;
  mC;
  value;

  slides: any;

  constructor(
    private lb: LoopbackService,
    private router: Router
  ) {
    this.isLoggedIn = this.lb.isLoggedIn();
    if (this.isLoggedIn === false) {
      this.router.navigate(['']);
    }
  }

  ngOnInit() {
    this.onProducts();
  }

  onProducts() {
    this.lb.getFindBy('carros', 'status', 'disponivel', 100, 'DESC')
    .subscribe(data => {
      this.slides = data;
    });
  }

  chunk(arr, chunkSize) {
    let R = [];
    for (let i = 0, len = arr.length; i < len; i += chunkSize) {
      R.push(arr.slice(i, i + chunkSize));
    }
    console.log(R);
    return R;
  }

  delete(id) {
    this.lb.getCountFindBy('carros', 'id', id)
    .subscribe(data => {
      if (data['count'] > 0) {
        console.log('Apago Iten');
        Swal.fire({
          title: 'Você tem certeza?',
          text: 'Você não poderá reverter isso!',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Sim, exclua!'
        }).then((result) => {
          this.delImages(id);
          this.delOpcionais(id);
          this.lb.deleteData('carros', id)
          .subscribe(car => {
            console.log('@@ Car Borrado ', car);
            this.onProducts();
            Swal.fire(
              'Excluído!',
              'Seu arquivo foi deletado.',
              'success'
            );
          });
        });
      } else {
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Something went wrong!',
          footer: '<a href>Why do I have this issue?</a>'
        });
        console.log('Sem Iten');
      }
    }, err => {
      console.log(err);
    });
  }

  getCarro() {
    this.lb.getFindBy('carros', 'status', 'disponivel').subscribe(datas => {
      this.count = datas['count'];
      this.carros = datas;
    });
  }

  private delImages(id) {
    console.log('@@ Imagess ID', id);
    this.lb.getFindBy('images', 'carroId', id)
    .subscribe(data => {
      console.log('@@ Images List', data);
      const k = Array(data);
      k.forEach((obj, index) => {
        console.log(`${obj['id']} - ${obj['nome']}`);
        this.lb.deleteData('images', obj['id'])
        .subscribe(q => console.log('@@@ Images Borrado', data));
        this.lb.deleteImg(obj['nome'])
        .subscribe(w => console.log('@@@ Imagem Arquivos Apagado', w));
      });
    });
  }

  private delOpcionais(id) {
    console.log('@@ Opcionais ID', id);
    this.lb.getFindBy('opcionais', 'carroId', id)
    .subscribe(d => {
      console.log('@@ Opcionais List', d);
      this.lb.deleteData('opcionais', d[0]['id'])
      .subscribe(data => {
        console.log('@@@ Opcionais Borrado', data);
      });
    });
  }

  onStatus(id) {
    this.lb.getFindBy('carros', 'id', id)
    .subscribe(u => {
      Swal.fire({
        title: 'Você tem certeza?',
        text: 'Vai a mudar status o carro',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sim, vou a mudar!',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if (result.value) {
          const form = {
            modeloCarro: u[0].modeloCarro,
            marcaCarro: u[0].marcaCarro,
            kilometragem: u[0].kilometragem,
            ano: u[0].ano,
            cor: u[0].cor,
            combustivel: u[0].combustivel,
            portas: u[0].portas,
            transmissao: u[0].transmissao,
            estado: u[0].estado,
            cidade: u[0].cidade,
            ofertas: u[0].ofertas,
            status: 'vendido',
            descricao: u[0].descricao,
            valor: u[0].valor,
          };
          this.lb.updateData('carros', id, form)
          .subscribe(data => this.getCarro());
        }
      });
    });
  }

  onOferta(id, offer) {
    this.lb.getFindBy('carros', 'id', id)
    .subscribe(u => {
      const form = {
        modeloCarro: u[0].modeloCarro,
        marcaCarro: u[0].marcaCarro,
        kilometragem: u[0].kilometragem,
        ano: u[0].ano,
        cor: u[0].cor,
        combustivel: u[0].combustivel,
        portas: u[0].portas,
        transmissao: u[0].transmissao,
        estado: u[0].estado,
        cidade: u[0].cidade,
        ofertas: offer,
        status: u[0].status,
        descricao: u[0].descricao,
        valor: u[0].valor,
      };
      this.lb.updateData('carros', id, form)
      .subscribe(data => {
        this.getCarro();
        this.ofertaModal.hide();
      });
    });
  }

  onValue(id, value) {
    this.lb.getFindBy('carros', 'id', id)
    .subscribe(u => {
      const form = {
        modeloCarro: u[0].modeloCarro,
        marcaCarro: u[0].marcaCarro,
        kilometragem: u[0].kilometragem,
        ano: u[0].ano,
        cor: u[0].cor,
        combustivel: u[0].combustivel,
        portas: u[0].portas,
        transmissao: u[0].transmissao,
        estado: u[0].estado,
        cidade: u[0].cidade,
        ofertas: u[0].ofertas,
        status: u[0].status,
        descricao: u[0].descricao,
        valor: value,
      };
      this.lb.updateData('carros', id, form)
      .subscribe(data => {
        this.getCarro();
        this.valorModal.hide();
      });
    });
  }

  onOfertaModal(id) {
    this.ofertaModal.show();
    this.lb.getFindBy('carros', 'id', id)
    .subscribe(data => {
      this.id = data[0].id;
      this.mC = data[0].marcaCarro;
      this.mc = data[0].modeloCarro;
    });
    this.lb.getTable('ofertas').subscribe(data => this.table = data);
  }

  onValorModal(id) {
    this.valorModal.show();
    this.lb.getFindBy('carros', 'id', id)
    .subscribe(data => {
      this.id = data[0].id;
      this.mC = data[0].marcaCarro;
      this.mc = data[0].modeloCarro;
      this.value = data[0].valor;
    });
  }
}
