import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { LoopbackService } from 'src/app/shared/service/loopback.service';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-passo2',
  templateUrl: './passo2.component.html',
  styleUrls: ['./passo2.component.scss']
})
export class Passo2Component implements OnInit {

  id: number;
  formOpcionais: FormGroup;
  carro;
  estado: string;
  cidade: string;
  oferta: string;
  isLoggedIn;

  constructor(
    private fb: FormBuilder,
    private lb: LoopbackService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.route.url.subscribe(u => {
      this.id = this.route.snapshot.params.id;
      this.onExits(this.id);
    });
    this.isLoggedIn = this.lb.isLoggedIn();
    if (this.isLoggedIn === false) {
      this.router.navigate(['']);
    }
  }

  ngOnInit() {
    this.lb.getFindBy('opcionais', 'carroId', this.id)
    .subscribe(data => {
      const o = data[0];
      this.formOpcionais = this.fb.group({
        arCondicionado: [o.arCondicionado],
        arBag: [o.arBag],
        travasEletricas: [o.travasEletricas],
        direcaoHidraulica: [o.direcaoHidraulica],
        brakeLight: [o.brakeLight],
        vidrosEletricos: [o.vidrosEletricos],
        limpadorTraseiro: [o.limpadorTraseiro],
        freioABS: [o.freioABS],
        radio: [o.radio],
        dvdPlayer: [o.dvdPlayer],
        vidrosTraseirosEletricos: [o.vidrosTraseirosEletricos],
        espelhosRetrovisoresTraseiros: [o.espelhosRetrovisoresTraseiros],
        bancosEnCouro: [o.bancosEnCouro],
        altosFalantes: [o.altosFalantes],
        gps: [o.gps],
        alarme: [o.alarme],
        protetorDeCacamba: [o.protetorDeCacamba],
        sensorEstaciomamentoTraseiro: [o.sensorEstaciomamentoTraseiro],
        carroId: [this.id]
      });
    });
  }

  getEstado(id) {
    this.lb.getFindBy('estados', 'id', id)
    .subscribe(data => this.estado = data[0]['nome']);
  }

  getCidade(id) {
    this.lb.getFindBy('cidades', 'id', id)
    .subscribe(data => this.cidade = data[0]['nome']);
  }

  getOferta(id) {
    this.lb.getFindBy('ofertas', 'id', id)
    .subscribe(data => this.oferta = data[0]['nome']);
  }

  onSend() {
    if (!this.formOpcionais.valid) {
      return;
    }

    const form = this.formOpcionais.value;
    this.lb.getFindBy('opcionais', 'carroId', this.id)
    .subscribe(d => {
      this.lb.updateData('opcionais', d[0].id, form)
      .subscribe(data => {
        Swal.fire(
          'Show!',
          'O Veículo foi atualizado',
          'success'
        );
      });
    });
  }

  onExits(carro) {
    this.lb.getFindBy('carros', 'id', this.id)
    .subscribe(data => {
      this.carro = data;
      this.getEstado(data[0].estado);
      this.getCidade(data[0].cidade);
      this.getOferta(data[0].ofertas);
    });
  }
}
