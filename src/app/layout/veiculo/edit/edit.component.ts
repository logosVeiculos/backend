import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'angular-bootstrap-md';
import { LoopbackService } from 'src/app/shared/service/loopback.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})

export class EditComponent implements OnInit {

  @ViewChild('basicModal') basicModal: ModalDirective;

  flag: boolean;
  estado: any;
  cidade: any;
  marca: any;
  opc: any;
  oferta: any;
  img1: any;
  formDados: FormGroup;
  opcionais: any;
  isLoggedIn;
  fmMarca: FormGroup;
  id: number;

  constructor(
    private fb: FormBuilder,
    private lb: LoopbackService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.route.url.subscribe(u => {
      this.id = this.route.snapshot.params.id;
    });
    this.flag = false;
    this.buildFormDados();
    this.isLoggedIn = this.lb.isLoggedIn();
    if (this.isLoggedIn === false) {
      this.router.navigate(['']);
    }
  }

  ngOnInit() {
    this.lb.getFindBy('carros', 'id', this.id)
    .subscribe(data => {
      const d = data[0];
      this.formDados = this.fb.group({
        modeloCarro: [d.modeloCarro, Validators.required],
        marcaCarro: [d.marcaCarro, Validators.required],
        cor: [d.cor, Validators.required],
        ano: [d.ano, Validators.required],
        kilometragem: [d.kilometragem, Validators.required],
        combustivel: [d.combustivel, Validators.required],
        portas: [d.portas, Validators.required],
        transmissao: [d.transmissao, Validators.required],
        estado: [d.estado, Validators.required],
        cidade: [d.cidade, Validators.required],
        ofertas: [d.ofertas, Validators.required],
        status: [d.status, Validators.required],
        valor: [d.valor, Validators.required],
        descricao: [d.descricao, Validators.required]
      });
    });



    this.getEstado();
    this.getOpcionais();
    this.getOfertas();
    this.getMarca();

  }

  buildFormDados() {
    this.fmMarca = this.fb.group({
      nome: ['', Validators.required]
    });
  }




  getEstado() {
    this.lb.getTable('estados')
    .subscribe((m) => { this.estado = m; });
  }

  getOfertas() {
    this.lb.getTable('ofertas')
    .subscribe((m) => { this.oferta = m; });
  }

  getOpcionais() {
    this.lb.getTable('opcionais')
    .subscribe((m) => { this.opc = m; });
  }

  getMarca() {
    this.lb.getTable('marcas')
    .subscribe(data => { this.marca = data; });
  }

  onGetCidade(event) {
    const edo = event.target.value;
    this.lb.getFindBy('cidades', 'estado', edo)
    .subscribe(data => { this.cidade = data; });
  }

  onSend() {
    if (!this.formDados.valid) {
      return;
    }
    const form = this.formDados.value;
    this.lb.updateData('carros', this.id, form)
    .subscribe(
      data => {
        console.log(data);
        Swal.fire(
          'Show!',
          'O Veículo foi atualizado',
          'success'
        );
      },
      err => {
        console.log(err);
      }
    );
  }

  onMarca() {
    this.lb.postData('marcas', this.fmMarca.value)
    .subscribe(data => {
      this.fmMarca.reset();
      this.getMarca();
      this.basicModal.hide();
    });
  }

  goPasso2() {
    this.router.navigate([`veiculos/edit/passo2/${this.id}`]);
  }
}
