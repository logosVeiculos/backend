import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoopbackService } from 'src/app/shared/service/loopback.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FileHolder } from 'angular2-image-upload';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-passo3',
  templateUrl: './passo3.component.html',
  styleUrls: ['./passo3.component.scss']
})
export class Passo3Component implements OnInit {


  id: number;
  carro: any;
  estado: string;
  cidade: string;
  oferta: string;
  modelo: string;
  marca: string;
  valor: string;
  cor: any;
  kilometragem: any;
  ano: any;
  portas: any;
  combustivel: any;
  transmissao: any;
  isLoggedIn;
  img;

  constructor(
    private fb: FormBuilder,
    private lb: LoopbackService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.route.url.subscribe(u => {
      this.id = this.route.snapshot.params.id;
      this.getImage(this.id);
      this.lb.getFindBy('carros', 'id', this.id).subscribe(data => {
        this.carro = data;
        this.modelo = data[0]['modeloCarro'];
        this.marca = data[0]['marcaCarro'];
        this.valor = data[0]['valor'];
        this.cor = data[0]['cor'];
        this.kilometragem = data[0]['kilometragem'];
        this.ano = data[0]['ano'];
        this.portas = data[0]['portas'];
        this.combustivel = data[0]['combustivel'];
        this.transmissao = data[0]['transmissao'];
        this.getEstado(data[0]['estado']);
        this.getCidade(data[0]['cidade']);
        this.getOferta(data[0]['ofertas']);
      });
    });
    this.isLoggedIn = this.lb.isLoggedIn();
    if (this.isLoggedIn === false) {
      this.router.navigate(['']);
    }
  }

  ngOnInit() {
  }


  onExits(carro) {
    this.lb.getCountFindBy('images', 'carroId', carro)
    .subscribe(
      count => {
        if (count > 1) {
          console.log(count);
          this.lb.getFindBy('carros', 'id', this.id).subscribe(data => {
            this.carro = data;
            this.getEstado(data[0].estado);
            this.getCidade(data[0].cidade);
            this.getOferta(data[0].ofertas);
          });
        } else {
          console.log('Cadastrado');
          this.router.navigate([`veiculos/passo3/${this.id}`]);
        }
      }
    );
  }

  getEstado(id) {
    this.lb.getFindBy('estados', 'id', id)
    .subscribe(data => this.estado = data[0]['nome']);
  }

  getCidade(id) {
    this.lb.getFindBy('cidades', 'id', id)
    .subscribe(data => this.cidade = data[0]['nome']);
  }

  getOferta(id) {
    this.lb.getFindBy('ofertas', 'id', id)
    .subscribe(data => this.oferta = data[0]['nome']);
  }

  onUploadFinished(file: FileHolder) {
    const form = {
      url: `http://api.compranalogos.com.br/api/storages/photos/download/${file['file']['name']}`,
      carroId: this.id,
      nome: file['file']['name']
    };
    this.lb.postData('images', form).subscribe();
  }

  onSend() {
    this.lb.getCountFindBy('images', 'carroId', this.id)
    .subscribe(count => {
      console.log(count);
      if (count['count'] > 5) {
        Swal.fire({
          title: 'Yeahhh!',
          text: 'Você terminou o registro do veículo',
          type: 'success',
          showCancelButton: true,
          confirmButtonColor: '#28a745',
          cancelButtonColor: '#3085d6',
          confirmButtonText: 'Vá ao painel!'
        }).then((result) => {
          this.router.navigate(['painel']);
        });
      } else {
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: `Actualmente a Agregado ${count['count']} imagens debe ser mas de 5!`,
          footer: 'Por que eu tenho esse problema?'
        });
      }
    });
  }

  getImage(id) {
    this.lb.getFindBy('images', 'carroId', id)
    .subscribe(data => {
      console.log('img', data);
      this.img = data;
    });
  }

  delImg(id, nome) {
    this.lb.deleteImg(nome).subscribe(data => {
      console.log(data);
      this.lb.deleteData('images', id).subscribe();
      Swal.fire(
        'Show!',
        'a imagem foi excluida',
        'success'
      ).then(() => this.getImage(this.id));
    });
  }
}
