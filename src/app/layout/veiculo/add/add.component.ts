import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'angular-bootstrap-md';
import { LoopbackService } from 'src/app/shared/service/loopback.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})

export class AddComponent implements OnInit {

  @ViewChild('basicModal') basicModal: ModalDirective;

  flag: boolean;
  estado: any;
  cidade: any;
  marca: any;
  opc: any;
  oferta: any;
  img1: any;
  formDados: FormGroup;
  opcionais: any;
  isLoggedIn;
  fmMarca: FormGroup;

  constructor(
    private fb: FormBuilder,
    private lb: LoopbackService,
    private router: Router
  ) {
    this.flag = false;
    this.buildFormDados();
    this.isLoggedIn = this.lb.isLoggedIn();
    if (this.isLoggedIn === false) {
      this.router.navigate(['']);
    }
  }

  ngOnInit() {
    this.getEstado();
    this.getOpcionais();
    this.getOfertas();
    this.getMarca();

  }

  buildFormDados() {
    this.formDados = this.fb.group({
      modeloCarro: ['', Validators.required],
      marcaCarro: ['', Validators.required],
      cor: ['', Validators.required],
      ano: ['', Validators.required],
      kilometragem: ['', Validators.required],
      combustivel: ['', Validators.required],
      portas: ['', Validators.required],
      transmissao: ['', Validators.required],
      estado: ['', Validators.required],
      cidade: ['', Validators.required],
      ofertas: ['', Validators.required],
      status: ['', Validators.required],
      valor: ['', Validators.required],
      descricao: ['', Validators.required]
    });
    this.fmMarca = this.fb.group({
      nome: ['', Validators.required]
    });
  }

  getEstado() {
    this.lb.getTable('estados')
    .subscribe((m) => { this.estado = m; });
  }

  getOfertas() {
    this.lb.getTable('ofertas')
    .subscribe((m) => { this.oferta = m; });
  }

  getOpcionais() {
    this.lb.getTable('opcionais')
    .subscribe((m) => { this.opc = m; });
  }

  getMarca() {
    this.lb.getTable('marcas')
    .subscribe(data => { this.marca = data; });
  }

  onGetCidade(event) {
    const edo = event.target.value;
    this.lb.getFindBy('cidades', 'estado', edo)
    .subscribe(data => { this.cidade = data; });
  }

  onSend() {
    if (!this.formDados.valid) {
      return;
    }
    const form = this.formDados.value;
    this.lb.postData('carros', form)
    .subscribe(
      data => {
        this.router.navigate([`veiculos/passo2/${data['id']}`]);
      },
      err => {
        console.log(err);
      }
    );
  }

  onMarca() {
    this.lb.postData('marcas', this.fmMarca.value)
    .subscribe(data => {
      this.fmMarca.reset();
      this.getMarca();
      this.basicModal.hide();
    });
  }
}
