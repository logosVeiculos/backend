import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MDBBootstrapModule, ModalModule } from 'angular-bootstrap-md';
import { AppRoutingModule } from './app-routing.module';
import { NgxSpinnerModule } from 'ngx-spinner';

import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/helper/header/header.component';
import { FooterComponent } from './shared/helper/footer/footer.component';
import { ImageUploadModule } from 'angular2-image-upload';
import {NgxPaginationModule} from 'ngx-pagination';
import { SigninComponent } from './layout/signin/signin.component';
import { SignupComponent } from './layout/signup/signup.component';
import { PainelComponent } from './layout/painel/painel.component';
import { GridComponent } from './layout/veiculo/grid/grid.component';
import { AddComponent } from './layout/veiculo/add/add.component';
import { ImagemComponent } from './layout/veiculos/imagem/imagem.component';
import { OpcionaisComponent } from './layout/veiculo/opcionais/opcionais.component';
import { ImagesComponent } from './layout/veiculo/images/images.component';
import { OfertasComponent } from './shared/component/ofertas/ofertas.component';
import { WimagenComponent } from './shared/component/wimagen/wimagen.component';
import { ViewComponent } from './layout/veiculo/view/view.component';
import { UpdateComponent } from './shared/component/update/update.component';
import { ChatStatusComponent } from './shared/component/chat-status/chat-status.component';
import { QuantidadeComponent } from './shared/component/quantidade/quantidade.component';
import { PercentageComponent } from './shared/component/percentage/percentage.component';
import { EditComponent } from './layout/veiculo/edit/edit.component';
import { Passo2Component } from './layout/veiculo/edit/passo2/passo2.component';
import { Passo3Component } from './layout/veiculo/edit/passo3/passo3.component';
import { BarChartComponent } from './shared/component/bar-chart/bar-chart.component';
import { UltimosProdutosComponent } from './shared/component/ultimos-produtos/ultimos-produtos.component';
import { CurrencyBrasilPipe } from './shared/pipe/currency-brasil.pipe';
import { InteressadoComponent } from './shared/component/interessado/interessado.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    SigninComponent,
    SignupComponent,
    PainelComponent,
    GridComponent,
    AddComponent,
    ImagemComponent,
    OpcionaisComponent,
    ImagesComponent,
    OfertasComponent,
    WimagenComponent,
    ViewComponent,
    UpdateComponent,
    ChatStatusComponent,
    QuantidadeComponent,
    PercentageComponent,
    EditComponent,
    Passo2Component,
    Passo3Component,
    BarChartComponent,
    UltimosProdutosComponent,
    CurrencyBrasilPipe,
    InteressadoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MDBBootstrapModule,
    ModalModule.forRoot(),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    ModalModule,
    ImageUploadModule.forRoot(),
    NgxSpinnerModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
