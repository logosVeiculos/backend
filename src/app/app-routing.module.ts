import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SigninComponent } from './layout/signin/signin.component';
import { PainelComponent } from './layout/painel/painel.component';
import { GridComponent } from './layout/veiculo/grid/grid.component';
import { AddComponent } from './layout/veiculo/add/add.component';
import { OpcionaisComponent } from './layout/veiculo/opcionais/opcionais.component';
import { ImagesComponent } from './layout/veiculo/images/images.component';
import { ViewComponent } from './layout/veiculo/view/view.component';
import { EditComponent } from './layout/veiculo/edit/edit.component';
import { Passo2Component } from './layout/veiculo/edit/passo2/passo2.component';
import { Passo3Component } from './layout/veiculo/edit/passo3/passo3.component';

const routes: Routes = [
  {path: '', component: SigninComponent},
  {path: 'signin', component: SigninComponent},
  {path: 'painel', component: PainelComponent},
  {path: 'veiculos', component: GridComponent},
  {path: 'veiculos/view/:id', component: ViewComponent},
  {path: 'veiculos/add', component: AddComponent},
  {path: 'veiculos/passo2/:id', component: OpcionaisComponent},
  {path: 'veiculos/passo3/:id', component: ImagesComponent},
  {path: 'veiculos/edit/:id', component: EditComponent},
  {path: 'veiculos/edit/passo2/:id', component: Passo2Component},
  {path: 'veiculos/edit/passo3/:id', component: Passo3Component},
  {path: '**', redirectTo: '/painel'}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
